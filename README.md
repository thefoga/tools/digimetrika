# Digimetrika

> Export, save, and analyze your digital data

# Why

## Problem

I feel like I'm wasting time with all this digitalization: computers have not alleviated us from hard and monotonous work, they just made us machines (of course it's not that simple, it's never the tool that's good or bad, it's how we use it). In particular, I think that we traded freedom for technology and progress (e.g reading a book under the sun on vacation? Bad. Working own ass off to have enough money to buy a tablet that *simulates* reading a book under the sun on vacation? Good.)

Of course some people think there is a problem some don't. Some people think they have a solution, some don't care, some are pessimists.

I'm a we-have-a-problem-but-I-dont-care type of guy: I think there is a big problem regarding our use of technology but I don't care to find a solution that *saves humanity*:

1. some think there is no problem at all, so why bother changing their mind?
2. I hate (stupid) people
3. I hate spending energy on a worthless task

## Motivation

- we have so many digital devices that "spy" on us and gather all possible data about us (again, neither good nor bad, it's simply a fact)
- even [Machiavelli](https://www.goodreads.com/book/show/1270379.The_Machiavellians) thought that humans are reluctant to do science on themselves
- there is a whole industry that has my Garmin data, a whole industry that has my Google data and so on. But they don't talk to each other. I can.

## Solution

So here is a solution to the useless usage of modern technology: why don't we use it to *actually* learn something about ourselves? e.g read on a Kindle? Why not have some statistics of what is the perfect time for me to read a book? (i.e the time when you have the least amount of interruptions and you can focus on the book?)

# What

## In detail

> use big-data analytics to discover more about how you behave

Thus I built some data-pipelines that (automatically) collect and analyze data about myself. This, I think, solves the problem of

- using all these devices that generate a lot of data about ourselves (that we don't use, since it's uploaded to the Apple/Google/Facebook servers instantly)
- analyzing my behavior (and saving psychiatric bills)

## Structure 

The abstract pipeline works like the following:

1. get source of data
2. save to persistent storage
3. ... do things normally for some time
4. analyze data

Thus, I need a *flexible* and *easy* way to

- create new pipelines
- get ahold of my data
- analyze it

## Case study: sleeping time

- I have a Garmin watch that monitors (the quality of) my sleep
- I have my browsing history

I want to see whether my browsing history influences my sleep. I don't trust anyone with my browsing history and my Garmin data.

What this work enables me to do is:

1. create a Garmin scraper that downloads all my sleeping history for the last year
2. download my browsing history
3. use some statistical test to see if any of my browsing habits influences my sleep (tl;qr yes, there is, it's the time of last website visit)
4. learn from that: use a (automatic) reminder that forces me to go sleep when it's best (e.g 11:30 PM)


# Caveat

Of course all of this is an example of finding *correlations* in your data: to have meaningful answers you'd run *causality inference* experiments on you (but that's complicated and not automatic).


# Inspiration

This work is inspired by

- Karl Pearson's Biometrika
- [The Personal Analytics of My Life](https://writings.stephenwolfram.com/2012/03/the-personal-analytics-of-my-life/)
- [Redshift sleep experiment](https://www.gwern.net/zeo/Redshift)
